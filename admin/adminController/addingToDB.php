<?php
require_once dirname(__DIR__) . "/adminModels/adminConfig.php";
require_once dirname(__DIR__) . "/adminModels/adminConnectDB.php";
if ($_POST['name'] != '' && $_POST['descrip'] != '' && is_uploaded_file($_FILES['image']['tmp_name']) && $_POST['price'] != '') {
    //запис картинки у папку pub/media/images
    $pathToFiles = $_SERVER['DOCUMENT_ROOT'] . "/pub/media/images/";
    $realPath = realpath($pathToFiles);
    $tempFileName = tempnam($realPath, "www");
    $imageNameAndExtension = explode(".", $_FILES['image']['name']);
    $a = explode('.', $tempFileName);
    $tempFileName = $a[0];
    $fileName = $tempFileName . "_{$imageNameAndExtension[0]}." . strtolower($imageNameAndExtension[1]);
    move_uploaded_file($_FILES['image']['tmp_name'], $fileName);
    $tempFileName .= '.' . $a[1];
    unlink($tempFileName);
    $fileName = explode("\\", $fileName);
    $fileName = array_pop($fileName);
    $fileName = 'media/images/' . $fileName;
    //запис нового товару у Базу Данних
    $query = "INSERT INTO `products` (image,name,description,price)
             VALUES ('" . $fileName . "' , '" . $_POST['name'] . "' , '" . $_POST['descrip'] . "' , '" . $_POST['price'] . "')";
    $pdo->query($query);
    header("Location: http://" . $_SERVER['HTTP_HOST'] . "/admin/admin.php?login={$_POST['login']}&pass={$_POST['pass']}&controller=insert.php");
} else header("Location: http://" . $_SERVER['HTTP_HOST'] . "/admin/admin.php?login={$_POST['login']}&pass={$_POST['pass']}&controller=insert.php&error=1");
