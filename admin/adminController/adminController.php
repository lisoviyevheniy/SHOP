<?php

class adminController
{
    protected $url;
    protected $mainURI;

    public function __construct()
    {
        if ($_GET['login'] != 'admin' && $_GET['pass'] != '12345')
            header('Location: http://' . $_SERVER["HTTP_HOST"] . '/templates/404.html');
    }

    public function getURI()
    {

        if (!isset($_GET['controller']))
            $this->url = 'adminHome.php';
        elseif (isset($_GET['controller']))
            $this->url = $_GET['controller'];
        return $this->url;
    }

    public function getClassName($fileName)
    {
        $a = explode(".", $fileName);
        return $a[0];
    }
}

?>