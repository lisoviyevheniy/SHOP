<?php
$pdo            = new PDO("mysql:host={$host}", $user, $pswd) or Die("error connect with $host");
$pdo->query("CREATE DATABASE IF NOT EXISTS `$databaseName`;");
$tryConnect     = $pdo->query("USE `$databaseName`");
$selectProducts = ("SELECT * FROM  `products`");
$tryProducts       = $pdo->query($selectProducts);
if (!$tryProducts)
    echo "<div>There is no table as products</div>";
unset($tryProducts);
