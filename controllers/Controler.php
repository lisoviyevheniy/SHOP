<?php

class Controller
{
    protected $url;

    public function __construct()
    {
        $a = explode('/', $_SERVER['REQUEST_URI']);
        $this->url = $a[2];
    }

    public function getURI()
    {
        if ($this->url == '')
            $this->url = 'home.php';
        if (strcmp($this->url, 'index.php') == 0)
            $this->url = 'home.php';
        return $this->url;
    }
}

