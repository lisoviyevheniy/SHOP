<?php

class autorization
{
    protected $query;
    protected $login;
    protected $password;

    public function __construct($path)
    {

        $a = explode('?', $path);
        if (isset($a[1]))
            $this->query = $a[1];
    }

    public function mainFunction()
    {

        $this->takeLogAndPass();
        if ($this->login != '' && $this->password != '') {
            if ($this->login == 'admin' && $this->password == '12345') {
                header('Location: http://' . $_SERVER["HTTP_HOST"] . '/admin/admin.php?login='
                    . $this->login . '&pass=' . $this->password);
            } else {
                //checking login and pass of other users ;
                header('Location: http://' . $_SERVER["HTTP_HOST"] . '/pub/');
            }
        } else header('Location: http://' . $_SERVER["HTTP_HOST"] . '/pub/');
    }

    protected function takeLogAndPass()
    {
        $a = explode('&', $this->query);
        if ($a[0] != '') {
            $this->login = explode('=', $a[0])[1];
            $this->password = explode('=', $a[1])[1];
        }
    }
}

?>
