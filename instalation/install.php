<?php
$pdo->query("CREATE TABLE IF NOT EXISTS `products`(
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            image VARCHAR(255) NOT NULL,
            name VARCHAR(40) NOT NULL,
            description TEXT,
            price INT(20)
            )");
$path = '';
$path .= dirname(__DIR__);
$path = str_replace("\\",'/',$path);
$pdo->query("INSERT INTO `products` (image,name,description,price)
             VALUES ('media/images/mobile1.jpg',
                   'Huawei Honor 4C White','Экран 5\" IPS (1280x720, сенсорный емкостный, Multi-Touch) /
                    моноблок / Hisilicon Kirin 620 (1.2 ГГц/ камера 13 Мп + фронтальная 5 Мп / Bluetooth 4.0
                    / Wi-Fi 802.11 b/g/n / 2 ГБ оперативной памяти /8 ГБ встроенной памяти + поддержка microSD
                    / разъем 3.5 мм / 3G / GPS / OC Android 5.1 /143.3 x 71.9 x 8.8 мм, 162 г / белый','4999')
                    " . ", ('media/images/Table1.jpg','LG 42LF650V',
                    'Диагональ экрана: 42\" Поддержка Smart TV: Есть Разрешение: 1920x1080 Wi-Fi: Да Диапазоны
                    цифрового тюнера: DVB-S2, DVB-C, DVB-T2 Частота обновления: PMI 550 Гц','17999')" . ",
                    ('media/images/TV1.jpg', 'Столик для ноутбука ', 'Бамбуковый
                    столик для ноутбука UFT Т-26. Является уникальной разработкой и имеет ряд преимуществ перед
                    деревянной копией этого столика: Значительно легче своего деревянного аналога. Более прочный,
                    поскольку древесина бамбука почти в полтора раза тверже дуба. Более экологичный, так как в
                    связи с быстрым ростом стебли не успевают накапливать вредные вещества.','1073')");
?>