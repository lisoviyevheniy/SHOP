<?php
$pdo            = new PDO("mysql:host={$host}", $user, $pswd) or Die("error connect with $host");
$pdo->query("CREATE DATABASE IF NOT EXISTS `$databaseName`;");
$tryConnect     = $pdo->query("USE `$databaseName`");
$selectProducts = ("SELECT * FROM  `products`");
$tryProducts    = $pdo->query($selectProducts);
if (!$tryProducts)
    require_once dirname(__DIR__)."/instalation/install.php";
unset($tryProducts);